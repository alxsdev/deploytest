FROM ubuntu:latest
USER ${ROOT}
RUN apt-get update && apt-get -yq upgrade \
      && apt-get -yq install gcc g++ autotools-dev apt-utils cmake wget \
      libhdf5-mpich-dev libopenblas-base libopenblas-dev libcurl4-openssl-dev 
      



#### TEST MPICH INSTALLATION ####
RUN mkdir /tmp/test

COPY mpi_hello_world.c /tmp/test/mpi_hello_world.c
COPY openmp.cpp /tmp/test/openmp.cpp
COPY test.sh /tmp/test/test.sh
WORKDIR /tmp/test
RUN sh test.sh
RUN rm -rf /tmp/test/*


#### CLEAN UP ####
WORKDIR /
RUN rm -rf /tmp/*


#### ADD DEFAULT USER ####
ARG USER=mpi
ENV USER ${USER}
RUN adduser --disabled-password ${USER} \
      && echo "${USER}   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

ENV USER_HOME /home/${USER}
RUN chown -R ${USER}:${USER} ${USER_HOME}

#### CREATE WORKING DIRECTORY FOR USER ####
ARG WORKDIR=/project
ENV WORKDIR ${WORKDIR}
RUN mkdir ${WORKDIR}
RUN chown -R ${USER}:${USER} ${WORKDIR}
ENV OMP_NUM_THREADS=4
WORKDIR ${WORKDIR}
USER ${USER}





CMD ["/bin/bash"]